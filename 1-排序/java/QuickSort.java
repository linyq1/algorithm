import java.util.Arrays;

/**
 * QuickSort
 * 快排
 * @author liuy
 * @data 2022/3/30 12:58
 **/


public class QuickSort {
    /**
     * 2 3 4 5 1 2 5 6 8 7 9 7
     * <br>
     * 1.找一个中间值，三点中值法，得到中间值,再与最左边边界交换值<br>
     * 2.双边分别扫描<br>
     *  <code>
     *      left   扫描大于  中间值pivot<br>
     *      right  扫描小于 中间值pivot<br>
     *  </code>
     * 3. 边界情况，当left==right 时 终止循环  pivot与right交换
     */
    //private static int arr[]=new int[]{2,1,3,4};
    private static int arr[]=new int[]{4, 1, 5, 3, 4, 1, 5, 0, 1, 7};

    public static void main(String[] args) {
        System.out.println(Arrays.toString(arr));
        sort(arr,0,arr.length-1);
        System.out.println(Arrays.toString(arr));
    }
    public static void sort(int[] arr,int begin,int end){
        if(begin>=end)return;
        int pivot=0;
        int left=begin;
        int right=end;
        pivot=partition(arr,left,right);

        while(left!=right){
            while(left<right&&arr[right]>pivot)right--;
            while(left<right&&arr[left]<=pivot)left++;

            if(left<right){
                swap(arr,left,right);
            }
        }
        swap(arr,begin,right);
        System.out.println(Arrays.toString(arr));
        sort(arr,begin,right-1);
        sort(arr,right+1,end);
    }
    private static int partition(int[] arr,int left,int right){
         int pivotIndex=left+((right-left)>>1);
         int pivot=0;
        if (arr[left]>arr[pivotIndex]&&arr[left]<arr[right]){
            pivot=arr[left];
            pivotIndex=left;
        }else if(arr[right]>arr[pivotIndex]&&arr[right]<arr[left]){
            pivot=arr[right];
            pivotIndex=right;
        }else{
            pivot=arr[pivotIndex];
        }
        swap(arr,left,pivotIndex);
        return pivot;
    }

    private static void swap(int[] arr, int left, int right) {
        if (left==right)return;
        arr[left]=arr[left]^arr[right];
        arr[right]=arr[left]^arr[right];
        arr[left]=arr[left]^arr[right];
    }

}
