import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * BubbleSort
 * 冒泡排序
 * @author liuy
 * @data 2022/3/17 17:47
 * 冒泡排序 相邻两个元素依次比较，大元素放后面
 **/


public class BubbleSort {
    public static void main(String[] args) throws Exception{
        bubbleSort(scanner());
    }

    public static void bubbleSort(int[] arr){
        //每轮比较次数
        int n=arr.length-1;
        //比较几轮
        int i=0;
        do{
            //最后一次交换位置
            int lastIndex=0;
            for (int j = 0; j < n; j++) {
                System.out.print("比较第 " + (j)+"次");
                if (arr[j]>arr[j+1]){
                    swap(arr,j,j+1);
                    lastIndex=j;
                }
                System.out.println(Arrays.toString(arr));
            }
            n=lastIndex;
            System.out.println("第" + (++i) + "轮" + Arrays.toString(arr)+"   lastIndex="+lastIndex);
        }while(n!=0);

        System.out.println("\n"+"最后结果==================");
        print(arr);
    }


    /**
     * 控制台输入数组
     * @return
     * @throws Exception
     */
    private static int[] scanner() throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        //获取输入长度
        int size = Integer.parseInt(br.readLine());
        //获取数组
        String[] nums = br.readLine().split(" ");
        int[] arr = new int[size];
        for(int i=0;i<size;i++) {
            arr[i] = Integer.parseInt(nums[i]);
        }
        return arr;
    }
    /**
     * 数组交换
     * @param arr
     * @param j
     * @param i
     */
    private static void swap(int[] arr,int j,int i){
        //a^a=0 a^0=a
        arr[i]=arr[i]^arr[j];
        arr[j]=arr[i]^arr[j];
        arr[i]=arr[i]^arr[j];
    }

    private static void print(int[] arr){
        for (int item : arr) {
            System.out.print(item+" ");
        }
    }
}
