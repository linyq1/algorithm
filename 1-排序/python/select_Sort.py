'''
# -*- coding: utf-8 -*- 
# @Time : 2022/3/16 下午2:48 
# @Author : linyq
# @File : select_Sort.py 
# @desc: 

    选择排序算法
'''
# ======== 输入部分 ========
len = int(input())
listarr = input().split(" ")


# ======== 算法部分 ========
def swap(arr, i , j):
    arr[i], arr[j] = arr[j], arr[i]

for i in range(len):
    minIndex = i
    for j in range(1+i, len):
        if int(listarr[i]) > int(listarr[j]):
            minIndex = j
            swap(listarr, i, j)
        else:
            minIndex = i

# ======== 输出部分 ========
for x in listarr:
    print(x, end=" ")