'''
# -*- coding: utf-8 -*- 
# @Time : 2022/3/16 下午4:58 
# @Author : linyq
# @File : insert_Sort.py 
# @desc: 

    插入排序
'''
# ======== 输入部分 ========
size = int(input())
# size = 4
arr = input().split(" ")
# arr = [3,2,1,4]

# ======== 算法部分 ========
def swap(arr, i , j):
    arr[i], arr[j] = arr[j], arr[i]

# 0-0是否有序，0-1， 0-2， 0-n
for i in range(1, size):    # 1,2,3,4 ... size
    j = i-1
    while j >= 0 and int(arr[j]) > int(arr[j+1]):
        swap(arr, j, j+1)
        j = j - 1


# ======== 输出部分 ========
for x in arr:
    print(x, end=" ")