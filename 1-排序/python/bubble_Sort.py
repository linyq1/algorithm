'''
# -*- coding: utf-8 -*- 
# @Time : 2022/3/16 下午3:50 
# @Author : linyq
# @File : bubble_Sort.py 
# @desc: 

    冒泡排序
'''
# ======== 输入部分 ========
len = int(input())
arr = input().split(" ")

# ======== 算法部分 ========
def swap(arr, i , j):
    arr[i], arr[j] = arr[j], arr[i]

while len > 0:
    len = len - 1
    for i in range(len):
        if int(arr[i]) > int(arr[i+1]):
            swap(arr, i, i+1)

# ======== 输出部分 ========
for x in arr:
    print(x, end=" ")
