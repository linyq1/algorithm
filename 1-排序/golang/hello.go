package main

import "fmt"

func main() {
    fmt.Println("===============")
    var x int = 111
    var y int = 222
    var z = max(x, y)
    fmt.Println(z)
    x, y = swap(x, y)
    fmt.Println(x, y)
}

func max(x, y int) int {
    if (x > y){
        return x
    } else {
        return y
    }
}

func swap(x, y int) (int, int){
    return y, x
}