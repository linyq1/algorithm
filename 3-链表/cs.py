
class Node:
    def __init__(self, item):
        self.item = item
        self.next = None


class SingleLinkList(object):
    def __init__(self):
        self._head = None

    def is_empty(self):
        return self._head is None

    def append(self, item):
        node = Node(item)
        if self.is_empty():
            self._head = node
        else:
            cur = self._head
            while cur.next is not None:
                cur = cur.next
            cur.next = node

    def items(self):
        cur = self._head
        while cur is not None:
            yield cur.item
            cur = cur.next

if __name__ == '__main__':
    list = SingleLinkList()
    for i in [1,2,3,4]:
        list.append(i)
    for i in list.items():
        print(i)
