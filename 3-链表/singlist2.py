'''
# -*- coding: utf-8 -*- 
# @Time : 2022/3/30 下午6:38 
# @Author : linyq
# @File : singlist2.py 
# @desc: 

'''
class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        n = len(nums)
        j = 0
        for i in range(n):
            if nums[i] != nums[j]:
                j+=1
                nums[j] = nums[i]
        return j+1
