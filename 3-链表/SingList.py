
class Node:
    def __init__(self, item):
        self.item = item
        self.next = None


class SingList:
    def __init__(self):
        self._head = None

    def isNone(self):
        return self._head is None

    def addend(self, item):
        node = Node(item)
        if self.isNone():
            self._head = node
        else:
            cur = self._head
            while cur.next is not None:
                cur = cur.next
            cur.next = node

    def view(self):
        cur = self._head
        ret = []
        while cur is not None:
            ret.append(cur.item)
            cur = cur.next
        return ret

    def distincts(self):
        cur = self._head
        while cur.next is not None:
            if cur.item == cur.next.item:
                cur.next = cur.next.next
            cur = cur.next


class Solution:
    def removeDuplicates(self, nums):
        list = SingList()
        for i in nums:
            list.addend(i)

        list.distincts()
        return len(list.view())

if __name__ == '__main__':
    a = Solution()
    a.removeDuplicates([1, 1, 2])
