'''
# -*- coding: utf-8 -*- 
# @Time : 2022/3/18 上午11:44 
# @Author : linyq
# @File : eor_search.py 
# @desc: 

    异或 算法
    一个数组中有一种数出现了奇数次，其他数都出现了偶数次，怎么找到这一个数？
        第一行输入一个n代表，有个n个长度大小的数组
        第二行输入一个长度为n的数组
'''
# ======== 输入部分 ========
size = int(input())
arr = list(map(int, input().split()))

# ======== 算法部分 ========
eor = 0
for i in arr:
    eor = eor ^ i

# ======== 输出部分 ========
print(eor)