'''
# -*- coding: utf-8 -*- 
# @Time : 2022/3/17 下午6:01 
# @Author : linyq
# @File : binary_search.py 
# @desc:

    二分法查找
'''
# ======== 输入部分 ========
size = input().split(" ")
arr = input().split(" ")
len = size[0]
tar = size[1]

# ======== 算法部分 ========
left = 0
right = int(len) - 1
ret = -1
while left <= right:
    mid = (right + left) // 2
    if int(arr[mid]) > int(tar):
        right = mid - 1
    elif arr[mid] < tar:
        left = mid + 1
    else:
        ret = mid
        break

# ======== 输出部分 ========
print(ret)
