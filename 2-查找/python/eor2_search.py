'''
# -*- coding: utf-8 -*- 
# @Time : 2022/3/18 上午11:55 
# @Author : linyq
# @File : eor2_search.py 
# @desc: 

    给定一个数字arr，其中只有两个数字出现了奇数次，其它数字都出现了偶数次，按照从小到大顺序输出这两个数。
        第一行输入一个n，
        第二行输入n个数
        输出出现奇数次的两个数，按照从小到大的顺序。
'''
# ======== 输入部分 ========
size = int(input())
arr = list(map(int, input().split()))

# ======== 算法部分 ========
eor = 0
for i in arr:
    eor = eor ^ i
right = eor & (~eor + 1) # 最右边的1
onleone = 0
for i in arr:
    if i & right != 0:
        onleone = onleone ^ i

# ======== 输出部分 ========
if onleone < onleone ^ eor:
    print(onleone, onleone ^ eor)
else:
    print(onleone ^ eor, onleone)
